(function() {
    $(document).ready(function() {

        $('#sidr-menu').sidr({
            side: 'right'
        });

        $(".menu-btn").click(function(){
            $(this).toggleClass('open');
        });

        $('#login-form-submit1').on('click', function (event) {
            var form = $("#login-form1");
            var input1 = form.find('#input1');
            var input2 = form.find('#input2');
            var send1 = form.find('#login-form-submit1');

            if (input1.val().length < 3) {
                $(input1).addClass('check-error');
                event.preventDefault();
            }

            if (input2.val().length < 3) {
                $(input2).addClass('check-error');
                event.preventDefault();
            }

            if (input1.val().length > 2 && input2.val().length > 2) {
                $(send1).addClass('btn-none').text(60);
                var timer = setInterval(function (){
                    var count = send1.text() - 1;
                    send1.html(count);

                    if (send1.text() <= 0){
                        clearInterval(timer);
                        send1.html("GET STARTED NOW");
                        $("#login-form-submit1").removeClass('btn-none');
                    }
                } ,1000);
            }

        });

        $('#login-form-submit2').on('click', function (event) {
            var form = $("#login-form2");
            var input3 = form.find('#input3');
            var input4 = form.find('#input4');
            var send2 = form.find('#login-form-submit2');

            if (input3.val().length < 3) {
                $(input3).addClass('check-error');
                event.preventDefault();
            }
            if (input4.val().length < 3) {
                $(input4).addClass('check-error');
                event.preventDefault();
            }

            if (input3.val().length > 2 && input4.val().length > 2) {
                $(send2).addClass('btn-none').text(60);
                var timer = setInterval(function (){
                    var count = send2.text() - 1;
                    send2.html(count);

                    if (send2.text() <= 0){
                        clearInterval(timer);
                        send2.html("GET STARTED NOW");
                        $("#login-form-submit2").removeClass('btn-none');
                    }
                } ,1000);
            }

        });


        var allFieldsLogin = $('#login-form1 #input1, #login-form1 #input2, #login-form2 #input3, #login-form2 #input4');

        $(allFieldsLogin).focus(function() {
            $(this).removeClass('check-error');
        });


    });
}).call(this);

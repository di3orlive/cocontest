(function() {
    $(document).ready(function() {

        $('#space').each(function(){
            var $this = $(this), numberOfOptions = $(this).children('option').length;

            $this.addClass('select-hidden');
            $this.wrap('<div class="select"></div>');
            $this.after('<div class="select-styled"></div>');

            var $styledSelect = $this.next('div.select-styled');
            $styledSelect.text($this.children('option').eq(0).text());

            var $list = $('<ul />', {
                'class': 'select-options'
            }).insertAfter($styledSelect);

            for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val()
                }).appendTo($list);
            }

            var $listItems = $list.children('li');

            $styledSelect.click(function(e) {
                e.stopPropagation();
                $('div.select-styled.active').each(function(){
                    $(this).removeClass('active').next('ul.select-options').hide();
                });
                $(this).toggleClass('active').next('ul.select-options').toggle();
            });

            $listItems.click(function(e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
                //console.log($this.val());
            });

            $(document).click(function() {
                $styledSelect.removeClass('active');
                $list.hide();
            });

        });



        if($(document).width() <= 750){
            $('.fix-item').appendTo('.fix-item-box');
        }
        else if($(document).width() >= 751){
            $('.fix-item2').appendTo('.fix-item-box');
        }
        $( window ).resize(function() {
            if($(document).width() <= 750){
                $('.fix-item').appendTo('.fix-item-box');
            }
            else if($(document).width() >= 751){
                $('.fix-item2').appendTo('.fix-item-box');
            }
        });



        $('#send').on('click', function (event) {
            var form = $(".form");
            var space = form.find('#space');
            var input1 = form.find('#input1');
            var input2 = form.find('#input2');
            var input3 = form.find('#input3');
            var input4 = form.find('#input4');
            var send = form.find('#send');
            var select = $('.select-styled').html();

            if (select === 'Choose kind of space') {
                $('.select-styled').addClass('check-error');
                event.preventDefault();
            }

            if (input1.val().length < 3) {
                $(input1).addClass('check-error');
                event.preventDefault();
            }

            if (input2.val().length < 3) {
                $(input2).addClass('check-error');
                event.preventDefault();
            }

            if (input3.val().length < 3) {
                $(input3).addClass('check-error');
                event.preventDefault();
            }

            if (input4.val().length < 3) {
                $(input4).addClass('check-error');
                event.preventDefault();
            }

            if (space.val().length > 2 && input1.val().length > 2 && input2.val().length > 2 && input3.val().length > 2 && input4.val().length > 2) {
                $(send).addClass('btn-none').text(60);
                var timer = setInterval(function (){
                    var count = send.text() - 1;
                    send.html(count);

                    if (send.text() <= 0){
                        clearInterval(timer);
                        send.html("get free tips!");
                        $("#send").removeClass('btn-none');
                    }
                } ,1000);
            }
        });


        var allFields = $('#space, #input1, #input2, #input3, #input4');

        $(allFields).focus(function() {
            $(this).removeClass('check-error');
        });

        $('.select-styled').click(function(){
            $(this).removeClass('check-error');
        });


    });
}).call(this);
